import chalk from "chalk";
import * as _ from "lodash";
import * as config from "../lib/config";
import * as logger from "../lib/logger";
import * as run from "../lib/run";

module.exports = {
    command : "update",
    describe : "run 'restic self-update' on an all selected hosts",
    handler,
};

// CLI Handler
export async function handler(argv) {
    let hasErrors: number = 0;

    if ( argv["dry-run"] ) {
        throw new Error("No --dry-run supported\n");
    }

    for ( const host of config.hosts() ) {
        logger.info(`Running self-update on host ${host.name}`);

        const rv = await run.ssh("restic self-update", host);

        hasErrors += rv.exitCode;

        if ( rv.exitCode ) {
            logger.info(`Host ${host.name}: ` + chalk.red.bold("Error"));
        }
        else {
            logger.info(`Host ${host.name}: ` + chalk.green.bold("Ok"));
        }
    }

    if ( hasErrors ) {
        throw new Error("Got errors during check execution\n");
    }
}
