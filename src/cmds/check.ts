import chalk from "chalk";
import * as _ from "lodash";
import * as config from "../lib/config";
import * as logger from "../lib/logger";
import * as run from "../lib/run";

module.exports = {
    command : "check",
    describe : "check restic installation on hosts",
    handler,
};

// CLI Handler
export async function handler(argv) {
    let hasErrors: number = 0;

    if ( argv["dry-run"] ) {
        throw new Error("No --dry-run supported\n");
    }

    logger.info("Checking all selected hosts");

    for ( const host of config.hosts() ) {
        const result = await run.ssh("restic version", host, { silent: true } );

        if ( result.stdout.match(/restic.*compiled with/) ) {
            const version = result.stdout.match(/(\d+\.\d+\.\d+)/);

            logger.info(`Host ${host.name}: ` + chalk.green.bold("Ok - " + version[1]));
        }
        else {
            hasErrors++;
            logger.info(`Host ${host.name}: ` + chalk.red.bold("Error"));
            logger.error(result.stdout + result.stderr);
        }
    }

    if ( hasErrors ) {
        throw new Error("Got errors during check execution\n");
    }
}
