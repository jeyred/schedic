#!/usr/bin/env node

import * as sms from "source-map-support";
import * as Yargs from "yargs";
import * as Config from "./lib/config";

sms.install();

// Turn off yargs broken i18n (e.g. nuking locale .json files
// when print help on missing options).
process.env.LC_ALL = "C";

// Declare all global options and load commands dynamically
// from ./cmds/ folder.
let argv = Yargs
    .strict(true)
    .usage("Usage: $0 command [global and/or command options]")
    .option("config", {
        alias : "c",
        default : "./schedic.yml",
        describe : "configuration file",
        global : true,
        type : "string",
    })
    .option("repo", {
        alias : "r",
        describe : "perform operations just for this repo",
        global : true,
        type : "array",
    })
    .option("host", {
        alias : "h",
        array : true,
        describe : "perform operations just for this host",
        global : true,
        type : "string",
    })
    .option("path", {
        alias : "p",
        describe : "perform operations just for this path",
        global : true,
        type : "array",
    })
    .option("sync-output", {
        describe : "print log immediately, even if stdout is redirected",
        global : true,
    })
    .option("dry-run", {
        describe : "execute nothing, just print commands",
        global : true,
    })
    .option("skip-prune", {
        describe : "skip pruning on forget command",
        global : true,
    })
    .middleware(Config.middleware)
    .commandDir("cmds")
    .demandCommand(1, "must provide a valid command")
    .completion("completion", "generate bash/zsh completion script")
    .help()
    .fail((msg, error, yargs) => {
        if ( error ) {
            if ( error.message.match(/\n$/) ) {
                const message = error.message.replace(/\n$/, "");
                console.error(message);
            }
            else {
                console.error(error);
            }
        }
        else {
            console.error(msg);
            console.error(yargs.help());
        }
        process.exit(1);
    })
    .argv;

// Stupid Yargs interface not using a method here
// makes it difficult to make tsc and tslint happy...
argv = argv;
