import * as _ from "lodash";

export interface AnyObject {
    [key: string]: any;
}

export function mergeObjects(base: AnyObject, add: AnyObject): AnyObject {
    if ( !base && add ) { return _.cloneDeep(add); }
    if ( !base && !add ) { return {}; }

    const result = _.cloneDeep(base);

    if ( !add ) { return result; }

    for ( const key of Object.keys(add) ) {
        result[key] = add[key];
    }

    return result;
}

export function quote(arg: string): string {
    if ( arg.match(/^[\w.\/_-]+$/) ) {
        return arg;
    }
    else if ( !arg.match(/\\/) && !arg.match(/'/) ) {
        return "'" + arg + "'";
    }
    else {
        return "$'" + arg.replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'";
    }
}

type ConsoleWrapState = {
    log: string,
    error: string,
    logFunc?: any,
    errorFunc?: any,
};

export function wrapConsole(state: ConsoleWrapState): void {
    state.logFunc = console.log;
    state.errorFunc = console.error;

    console.log = (line) => state.log += line;
    console.error = (line) => state.error += line;
}

export function unWrapConsole(state: ConsoleWrapState): void {
    console.log = state.logFunc;
    console.error = state.errorFunc;
}
