import * as _ from "lodash";
import * as config from "../lib/config";
import * as util from "../lib/util";

// Disable restic environment variables because they
// will override settings given at the command line
delete process.env.RESTIC_PASSWORD;
delete process.env.RESTIC_PASSWORD_FILE;
delete process.env.RESTIC_REPOSITORY;

// Merge two restic option objects
export function mergeOptions(base: config.ResticOptions, add: config.ResticOptions): config.ResticOptions {
    return util.mergeObjects(base as util.AnyObject, add as util.AnyObject) as config.ResticOptions;
}

// Stringify restic options
function stringifyOptions(options: config.ResticOptions): string {
    if ( !options ) { return ""; }

    let optionsString: string = "";

    for ( const option of Object.keys(options) ) {
        if ( options[option] === null || options[option] === true ) {
            optionsString += " --" + option;
        }
        else if ( options[option] !== false ) {
            optionsString += " --" + option + " '" + options[option] + "'";
        }
    }

    return optionsString;
}

// Build restic backup command
export function backupCommand(backup: config.Backup, repo: config.Repo): string {
    const excludes = backup.exclude ? " " +
        _.map(backup.exclude, (exc) => "-e " + util.quote(exc))
         .join(" ") : " ";

    const tags = backup.tags ? " " +
        (Array.isArray(backup.tags) ?
            _.map(backup.tags, (tag) => "--tag " + util.quote(tag))
             .join(" ") : "--tag " + util.quote(backup.tags) ) : " ";

    const options = stringifyOptions(mergeOptions(repo.restic, backup.restic));

    const oneFileSystem = backup["one-file-system"] ? " -x" : "";

    return `restic -r ${repo.url} backup ` +
           `${options}${oneFileSystem}${excludes}${tags} ` +
           util.quote(backup.path);
}

// Build restic forget command
export function forgetCommand(backup: config.Backup, repo: config.Repo): string {
    const host = config.host(backup.hostName);

    let keepOptions: string = "";

    for ( const keep of Object.keys(backup.keep) ) {
        keepOptions += " --keep-" + keep + " " + backup.keep[keep];
    }

    const options = stringifyOptions(repo.restic);

    return `restic -r ${repo.url} forget${options} ` +
           `--host ${host.name} --path ` + util.quote(backup.path) +
           `${keepOptions}`;
}

// Build restic prune command
export function pruneCommand(repo: config.Repo): string {
    const options = stringifyOptions(repo.restic);

    return `restic -r ${repo.url} prune${options} `;
}
