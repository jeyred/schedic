import { assert } from "chai";
import * as _ from "lodash";
import * as os from "os";
import * as config from "./config";

export async function assertThrowsAsync(fn, regExp) {
    let f = () => { return; };

    try {
        await fn();
    }
    catch (e) {
        f = () => { throw e; };
    }
    finally {
        assert.throws(f, regExp);
    }
}

export function arrayContains(array: string[], regex): boolean {
    return !!_.find(array, (el) => el.match(regex));
}

export function patchConfigHostname(configHostName: string, argv: any) {
    const hostName = os.hostname();
    config.host(configHostName).name = hostName;
    argv.host = [ hostName ];
    config.backups().forEach( (b) => b.hostName = hostName );
}
