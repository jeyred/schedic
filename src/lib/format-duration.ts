export default function formatDuration(msec) {
    const sec = Math.floor(msec / 1000);
    const h = Math.floor(sec / 60 / 60);
    const m = Math.floor((sec - h * 3600) / 60);
    const s = sec - h * 3600 - m * 60;

    return ("0" + h).slice(-2) + ":" +
           ("0" + m).slice(-2) + ":" +
           ("0" + s).slice(-2);
}
