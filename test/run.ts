import { assert } from "chai";
import "mocha";
import * as config from "../src/lib/config";
import * as logger from "../src/lib/logger";
import * as run from "../src/lib/run";

logger.silent();

describe("run.command()", () => {
    it("should run node --version", async () => {
        const rv = await run.command("node --version", { silent: true } );
        assert.equal(rv.exitCode, 0, "node --version exitCode 0");
        assert.match(rv.stdout, /^v/, "Node version");
    });

    it("should feed process stdin", async () => {
        const rv = await run.command("cat", { silent: true, input: "Hello stdin" } );
        assert.equal(rv.exitCode, 0, "cat stdin exitCode 0");
        assert.match(rv.stdout, /^Hello stdin/, "Hello stdin");
    });

    it("should catch process stderr", async () => {
        const rv = await run.command(">&2 echo Hello stderr", { silent: true } );
        assert.equal(rv.exitCode, 0, "cat stderr exitCode 0");
        assert.match(rv.stderr, /^Hello stderr/, "Hello stderr");
    });

    it("should return exitCode", async () => {
        const rv = await run.command("exit 42", { silent: true } );
        assert.equal(rv.exitCode, 42, "exitCode 42");
    });

    it("should log process stdout", async () => {
        logger.buffer();
        logger.clearBuffer();
        const rv = await run.command("cat", { input: "Hello stdout" } );
        assert.equal(rv.exitCode, 0, "exitCode 0");
        assert.match(logger.getBuffer()[0], /Hello stdout/, "console log stdout");
    });

    it("should log process stderr", async () => {
        logger.buffer();
        logger.clearBuffer();
        const rv = await run.command(" echo Hello stderr >&2; exit 42");
        assert.equal(rv.exitCode, 42, "exitCode 42");
        assert.match(logger.getBuffer()[1], /Hello stderr/, "console log stderr");
    });
});

describe("run.ssh()", () => {
    it("should run command with ssh=echo", async () => {
        const host: config.Host = {
            backups: [],
            name: "foo",
            ssh: "echo",
        };

        const rv = await run.ssh("command", host, { silent: true } );

        assert.match(rv.stdout, /root@foo.*command/);
        assert.equal(rv.exitCode, 0);
    });

    it("should run command with ssh=false", async () => {
        const host: config.Host = {
            backups: [],
            name: "foo",
            ssh: false,
        };

        const rv = await run.ssh("echo some command", host);

        assert.match(rv.stdout, /some command/);
        assert.equal(rv.exitCode, 0);
    });
});

describe("run.restic()", () => {
    it("should run command with password", async () => {
        const host: config.Host = {
            backups: [],
            name: "foo",
            ssh: false,
        };

        const repo: config.Repo = {
            name: "local",
            password: "tell me the secret",
            url: "/tmp/repo",
        };

        let rv = await run.restic("cat", host, repo, { silent: true } );

        assert.equal(rv.exitCode, 0);
        assert.match(rv.stdout, /tell me the secret/);

        rv = await run.restic("cat", host, repo);

        assert.equal(rv.exitCode, 0);
        assert.match(rv.stdout, /tell me the secret/);
    });

    it("should run command with environment", async () => {
        const host: config.Host = {
            backups: [],
            env: { FOO: "SOME BAR" },
            name: "foo",
            ssh: false,
        };

        const repo: config.Repo = {
            name: "local",
            password: "tell me the secret",
            url: "/tmp/repo",
        };

        const rv = await run.restic("cat; echo $FOO", host, repo, { silent: true } );

        assert.equal(rv.exitCode, 0);
        assert.match(rv.stdout, /SOME BAR/);
        assert.match(rv.stdout, /tell me the secret/);
    });
});
