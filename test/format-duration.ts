import { assert } from "chai";
import "mocha";
import formatDuration from "../src/lib/format-duration";

describe("formatDuration()", () => {
    it("should format durations correctly", () => {
        assert.equal(formatDuration(0), "00:00:00", "0 seconds");
        assert.equal(formatDuration(1000), "00:00:01", "1 second");
        assert.equal(formatDuration(59000), "00:00:59", "59 seconds");
        assert.equal(formatDuration(60000), "00:01:00", "60 seconds");
        assert.equal(formatDuration(67000), "00:01:07", "67 seconds");
        assert.equal(formatDuration(10 * 60 * 1000), "00:10:00", "10 minutes");
        assert.equal(formatDuration(59 * 60 * 1000 + 59000), "00:59:59", "just under an hour");
        assert.equal(formatDuration(90 * 60 * 1000), "01:30:00", "one hour and a half");
    });
});
