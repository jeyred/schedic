import { assert } from "chai";
import * as _ from "lodash";
import "mocha";
import * as backupCmd from "../src/cmds/backup";
import * as checkCmd from "../src/cmds/check";
import * as forgetCmd from "../src/cmds/forget";
import * as repoCmd from "../src/cmds/repo";
import * as runCmd from "../src/cmds/run";
import * as updateCmd from "../src/cmds/update";
import * as config from "../src/lib/config";
import * as logger from "../src/lib/logger";
import * as run from "../src/lib/run";
import * as util from "../src/lib/util";
import { arrayContains, assertThrowsAsync, patchConfigHostname } from "../src/lib/test-util";

logger.silent();

describe("command backup", () => {
    it("should dry run restic backup of host ci1", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci1"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await backupCmd.handler(argv);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /All tasks successfully completed/));
    });

    it("should perform restic backup of host ci1", async function() {
        if ( !process.env.RESTIC_TEST ) {
            this.skip();
            return;
        }

        this.timeout(10000);

        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci1"],
            repo: ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await backupCmd.handler(argv);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /All tasks successfully completed/));
    });

    it("should get errors of host ci-error", async function() {
        if ( !process.env.RESTIC_TEST ) {
            this.skip();
            return;
        }

        this.timeout(10000);

        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci-error"],
            repo: ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await assertThrowsAsync(async () => {
            await backupCmd.handler(argv);
        }, /Got errors during backup execution/);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /errpath-eichaijee6xaez4ooseiShie does not exist/));
    });
});

describe("command forget", () => {
    it("should dry run forget with host ci2", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await forgetCmd.handler(argv);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /All tasks successfully completed/));
    });

    it("should perform forget with host ci2", async function() {
        if ( !process.env.RESTIC_TEST ) {
            this.skip();
            return;
        }

        this.timeout(10000);

        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci2"],
            repo: ["local"],
        };

        await config.middleware(argv);

        // Patch hostname, otherwise restic doesn't match it
        patchConfigHostname("ci2", argv);

        // Perform another backup (so we have double snapshots)
        await backupCmd.handler(argv);

        // Verify snapshots
        let rv = await run.command("restic -r tmp/restic --password-file tmp/pw snapshots");
        assert.match(rv.stdout, /4 snapshots/);

        logger.clearBuffer();
        logger.buffer();

        // Now apply forget
        await forgetCmd.handler(argv);

        const buffer = logger.getBuffer();
        assert.ok(arrayContains(buffer, /All tasks successfully completed/));

        // Verify result
        rv = await run.command("restic -r tmp/restic --password-file tmp/pw snapshots");
        assert.match(rv.stdout, /2 snapshots/);
    });
});

describe("command run", () => {
    it("should dry run 'run'", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await runCmd.handler(argv);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /All tasks successfully completed/));
        assert.ok(arrayContains(buffer, /Backup summary/));
        assert.ok(arrayContains(buffer, /Forget summary/));
        assert.ok(arrayContains(buffer, /Prune summary/));
        assert.ok(!arrayContains(buffer, /Got errors/));
    });
});

describe("command update", () => {
    it("should complain about dry-run option", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await assertThrowsAsync(async () => {
            await updateCmd.handler(argv);
        }, /No --dry-run supported/);
    });

    it("should perform restic self-update", async function() {
        if ( !process.env.RESTIC_TEST ) {
            this.skip();
            return;
        }

        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci2"],
            repo: ["local"],
        };

        this.timeout(10000);

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await updateCmd.handler(argv);

        const buffer = logger.getBuffer();

        assert.ok(arrayContains(buffer, /writing restic to/));
    });

    it("should fail on unknown host", async function() {
        const argv = {
            config: "test/assets/backup.yml",
            host: ["unknown.haiqueeyuumuc3ti6eegeiki6ifahw2y.de"],
            repo: ["local"],
        };

        this.timeout(10000);

        await config.middleware(argv);

        await assertThrowsAsync(async () => {
            await updateCmd.handler(argv);
        }, /Got errors during check execution/);
    });
});

describe("command check", () => {
    it("should complain about dry-run option", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await assertThrowsAsync(async () => {
            await checkCmd.handler(argv);
        }, /No --dry-run supported/);
    });

    it("should perform restic check", async function() {
        const argv = {
            config: "test/assets/backup.yml",
            host: ["ci2"],
            repo: ["local"],
        };

        this.timeout(10000);

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await checkCmd.handler(argv);

        const buffer = logger.getBuffer();
        assert.ok(arrayContains(buffer, /ci2.*Ok/));
    });

    it("should fail with ssh on unknown host", async function() {
        const argv = {
            config: "test/assets/backup.yml",
            host: ["unknown.haiqueeyuumuc3ti6eegeiki6ifahw2y.de"],
            repo: ["local"],
        };

        this.timeout(10000);

        await config.middleware(argv);

        logger.clearBuffer();
        logger.buffer();

        await assertThrowsAsync(async () => {
            await checkCmd.handler(argv);
        }, /Got errors during check execution/);

        const buffer = logger.getBuffer();
        assert.ok(arrayContains(buffer, /unknown.haiqueeyuumuc3ti6eegeiki6ifahw2y.de.*Error/));
    });
});

describe("command repo", () => {
    it("should print environment for repo 'local'", async () => {
        const argv = {
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
            "repo": ["local"],
        };

        await config.middleware(argv);

        const state = { log: "", error: "" };
        util.wrapConsole(state);

        repoCmd.handler(argv);

        util.unWrapConsole(state);

        assert.equal(state.log, "export RESTIC_PASSWORD='12345'; export RESTIC_REPOSITORY='tmp/restic'");
    });

    it("should accept repo param as positional argument", async () => {
        const argv = {
            "_": ["repo", "local"],
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
        };

        await config.middleware(argv);

        const state = { log: "", error: "" };
        util.wrapConsole(state);

        repoCmd.handler(argv);

        util.unWrapConsole(state);

        assert.equal(state.log, "export RESTIC_PASSWORD='12345'; export RESTIC_REPOSITORY='tmp/restic'");
    });

    it("should complain about too many arguments", async () => {
        const argv = {
            "_": ["repo", "local", "foo"],
            "config": "test/assets/backup.yml",
            "dry-run": true,
            "host": ["ci2"],
        };

        await config.middleware(argv);

        assert.throw(() => repoCmd.handler(argv), /Too many arguments/);
    });

    it("should complain about missing repo argument", async () => {
        const argv = {
            "config": "schedic.yml",
            "dry-run": true,
        };

        await config.middleware(argv);

        assert.throw(() => repoCmd.handler(argv), /Missing repo argument/);
    });
});
