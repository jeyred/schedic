import { assert } from "chai";
import "mocha";
import * as run from "../src/lib/run";

describe("schedic cli", () => {
    it("should recognize all allowed options", async function() {
        const cmd =
            "node build/schedic.js forget " +
            "--config test/assets/backup.yml " +
            "--repo local " +
            "--host ci1 " +
            "--path /tmp/schedic/1 " +
            "--sync-output " +
            "--skip-prune " +
            "--dry-run";

        this.timeout(10000);

        const rv = await run.command(cmd);

        assert.equal(rv.exitCode, 0);
    });

    it("should complain about unknown options", async function() {
        const cmd =
            "node build/schedic.js forget " +
            "--config test/assets/backup.yml " +
            "--unknownoption " +
            "--dry-run";

        // But unknown options with dashes pass 'strict' mode.
        // Known yet unfixed but in Yargs:
        // https://github.com/yargs/yargs/issues/1039

        this.timeout(10000);

        const rv = await run.command(cmd);

        assert.notEqual(rv.exitCode, 0);
        assert.match(rv.stderr, /Unknown argument: unknownoption/);
    });

    it("should complain about unknown command", async function() {
        const cmd =
            "node build/schedic.js thiswillneverwork ";

        this.timeout(10000);

        const rv = await run.command(cmd);

        assert.notEqual(rv.exitCode, 0);
        assert.match(rv.stderr, /Unknown argument: thiswillneverwork/);
    });
});
