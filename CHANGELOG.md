# CHANGELOG for schedic

## 0.9.7 (2019-04-13)

### Fixes

- crash when installed as project dependency

## 0.9.6 (2019-04-08)

### Fixes

- added missing production dependency

## 0.9.5 (2019-03-24)

### Fixes

- a production module required a dev dependency
- build process now makes cli script executable

## 0.9.4 (2019-03-24)

### Notes

- bash is now required on schedic and target hosts

### Fixes

- shell quoting issues (special chars in paths etc.)
- unset RESTIC_* environment variables to avoid
  confusion with the password and command line
  options passed by schedic
- fixed log messages tagged "ERR" by accident

### Features

- implemented repo command
- added environment configuration and passing
- this bumped schedic.yml version to 2.0

## 0.9.3 (2019-03-20)

### Fixes

- host-defaults ssh option wasn't recognized

### Features

- no user visible new features
- added unit tests and increased overall code quality

## 0.9.2 (2019-03-16)

### Fixes

- made logo URL absolute to fix broken image on npmjs.com

## 0.9.1 (2019-03-16)

### Fixes

- logo and example yml missed in npm registry publish 

## 0.9.0 (2019-03-16)

### Features

- first public release
- implemented commands
    - backup
    - check
    - forget
    - run
    - update
- configuration version 1.0
