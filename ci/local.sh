#!/bin/bash
set -e

if [ ! -e /.dockerenv ]
then
    exec docker run --rm -v $PWD:/work -w /work \
        -u $(id -u):$(id -g) \
        node:10 ci/local.sh
fi

ci/lint.sh
ci/build.sh
ci/test.sh
